# Brain Corp - Lidar Filter

* Name: Kevin Diep

* Email: kevindiep@utexas.edu

* GitSHA: 9e3a2c7480c3be4601f88ff643f93708c95ae611

* GitLab Code Repo: https://gitlab.com/kd24972/lidar-filter

* GitLab Pipelines: https://gitlab.com/kd24972/lidar-filter/-/pipelines

* Estimated Completion Time: 8 hours

* Actual Completion Time: 12 hours

* Comments: Lidar Filter implementation written inside LidarFilter.cpp and 
LiderFilter.hpp. RunLidarFilter.cpp creates either a range filter or temporal 
mean filter based on the input assuming the first line gives the number of 
previous scans to store and runs the filter. Acceptance tests written in 
TestLidarFilter.cpp and unit tests for both range and temporal median filter 
can be found on the public 
[test repo](https://gitlab.com/kd24972/lidar-filter-tests). A git log of all 
commits are in LidarFilter.log and documentation to the interfaces can be 
found in the html folder. A list of 
[issues](https://gitlab.com/kd24972/lidar-filter/-/issues) created and 
resolved throughout the project can be found in the repo. 
