#include <iostream> // istream, ostream

using namespace std;

// General Lidar Filter class
class LidarFilter {
public:
    // length of each scan
    int n = -1;
    // number of scans read
    int time = 0;
    // finds the length, N
    void read_length(string);
    // reads values into scan array
    void read_scan(string, float (&scan)[]);
    // prints the time, input, and solution
    void print_filter(ostream&, float (&scan)[], float (&filter)[]);
};

// Filter that crops values below and above the range min/max
// with the range's min/max and is returned through filter
class RangeFilter: public LidarFilter {
public:
    // Range of distances
    const float range_min = 0.03;
    const float range_max = 50.0;
    // Returns the updated scan through filter
    void update(float (&scan)[], float (&filter)[]);
    void solve (istream&, ostream&);
};

// Filter that finds the median of the current and previous d
// scans stored in the cache and is returned through filter
class TMFilter: public LidarFilter {
public:
    // number of previous scans
    int d;
    TMFilter(int num_scans);
    // Reads in the input and previous scan to find the median distance
    void update(float (&scan)[], float **(&prev_scans), float (&filter)[]);
    // Updates the cache with previous d distances
    void update_prevs(float (&scan)[], float **(&prev_scans));
    void solve (istream&, ostream&);
};
