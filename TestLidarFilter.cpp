#include <sstream>  // istringtstream, ostringstream

#include "gtest/gtest.h"

#include "LidarFilter.hpp"

using namespace std;

// Testing the length of the scan
TEST(LidarFilterFixture, read_length) {
    LidarFilter lf;
    lf.read_length("[0., 1., 2., 1., 3.]\n");
    ASSERT_EQ(lf.n, 5);
}

// Testing scan values
TEST(LidarFilterFixture, read_scan) {
    LidarFilter lf;
    string s = "[0., 1., 2., 1., 3.]\n";
    lf.read_length(s);
    float scan[lf.n] = {};
    lf.read_scan(s, scan);
    float expected_output [5] = {0.0, 1.0, 2.0, 1.0, 3.0};
    for (int i = 0; i < 5; ++i) {
        ASSERT_EQ(scan[i], expected_output[i]);
    }
}

// Testing range filter's min update
TEST(RangeFilterFixture, update_min) {
    RangeFilter rf;
    string s = "[0., 1., 2., 1., 3.]\n";
    rf.read_length(s);
    float scan[rf.n] = {};
    rf.read_scan(s, scan);
    float filter[rf.n] = {};
    rf.update(scan, filter);
    float expected_output [5] = {1.0, 1.0, 2.0, 1.0, 3.0};
    for (int i = 0; i < 5; ++i) {
        ASSERT_EQ(filter[i], expected_output[i]);
    }
}

// Testing range filter's max update
TEST(RangeFilterFixture, update_max) {
    RangeFilter rf;
    string s = "[55., 45., 4., 0., 0.]\n";
    rf.read_length(s);
    float scan[rf.n] = {};
    rf.read_scan(s, scan);
    float filter[rf.n] = {};
    rf.update(scan, filter);
    float expected_output [5] = {45.0, 45.0, 4.0, 4.0, 4.0};
    for (int i = 0; i < 5; ++i) {
        ASSERT_EQ(filter[i], expected_output[i]);
    }
}

// testing number of previous scans
TEST(TMFilterFixture, num_prev_scans) {
    TMFilter tmf(3);
    ASSERT_EQ(tmf.d, 3);
}

// testing cache of previous values
TEST(TMFilterFixture, update_prevs1) {
    TMFilter tmf(3);
    string s = "[0., 1., 2., 1., 3.]\n";
    tmf.read_length(s);
    float scan[tmf.n] = {};
    tmf.read_scan(s, scan);
    float **prev_scans;
    prev_scans = new float *[tmf.n];
    for (int i = 0; i < tmf.n; ++i) {
        prev_scans[i] = new float[tmf.d];
    }
    tmf.update_prevs(scan, prev_scans);
    float expected_output[][2] = {{0.0}, {1.0}, {2.0}, {1.0}, {3.0}};
    for (int j = 0; j < tmf.n; ++j) {
        ASSERT_EQ(prev_scans[j][0], expected_output[j][0]);
    }
}

// testing cache of previous values with two scans
TEST(TMFilterFixture, update_prevs2) {
    TMFilter tmf(3);
    string s = "[0., 1., 2., 1., 3.]\n";
    tmf.read_length(s);
    float scan[tmf.n] = {};
    tmf.read_scan(s, scan);
    float **prev_scans;
    prev_scans = new float *[tmf.n];
    for (int i = 0; i < tmf.n; ++i) {
        prev_scans[i] = new float[tmf.d];
    }
    tmf.update_prevs(scan, prev_scans);
    string s2 = "[1., 5., 7., 1., 3.]\n";
    tmf.read_scan(s2, scan);
    tmf.time = 1;
    tmf.update_prevs(scan, prev_scans);
    float expected_output[][2] = {{0.0, 1.0}, {1.0, 5.0}, {2.0, 7.0}, {1.0, 1.0}, {3.0, 3.0}};
    for (int j = 0; j < tmf.n; ++j) {
        for (int k = 0; k < tmf.time + 1; ++k) {
            ASSERT_EQ(prev_scans[j][k], expected_output[j][k]);
        }
    }
}

// Testing range filter's solve
TEST(RangeFilterFixture, solve) {
    RangeFilter rf;
    istringstream sin("[0., 1., 2., 1., 3.]\n[1., 5., 7., 1., 3.]");
    ostringstream sout;
    rf.solve(sin, sout);
    ASSERT_EQ(sout.str(), "0 [0, 1, 2, 1, 3] [1, 1, 2, 1, 3]\n1 [1, 5, 7, 1, 3] [1, 5, 7, 1, 3]\n");
}

// Testing temporal median filter's solve
TEST(TMFilterFixture, solve) {
    TMFilter tmf(3);
    istringstream sin("[0., 1., 2., 1., 3.]\n[1., 5., 7., 1., 3.]\n");
    ostringstream sout;
    tmf.solve(sin, sout);
    ASSERT_EQ(sout.str(), "0 [0, 1, 2, 1, 3] [0, 1, 2, 1, 3]\n1 [1, 5, 7, 1, 3] [0.5, 3, 4.5, 1, 3]\n");
}

